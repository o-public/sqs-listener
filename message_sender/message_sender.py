from sqs_launcher import SqsLauncher


class MessageSender:
    def __init__(self, app, queue_name):
        self.queue_name = queue_name
        self.app = app
        self.output_queue = SqsLauncher(self.queue_name)

    def __send_internal(self, message):
        self.output_queue.launch_message(message)

    def success_message(self, params):
        return {
            'app': self.app,
            'result': 'success',
            'params': params
        }

    def error_message(self, params, message):
        return {
            'app': self.app,
            'result': 'error',
            'message': message,
            'params': params
        }

    def health_message(self, params):
        return {
            'app': self.app,
            'result': 'health',
            'params': params
        }

    def success(self, body):
        self.__send_internal(self.success_message(body))

    def error(self, body, message):
        self.__send_internal(self.error_message(body, message))

    def health(self, body):
        self.__send_internal(self.health_message(body))
